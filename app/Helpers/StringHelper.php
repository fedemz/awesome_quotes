<?php

function compareStringsUrl($string_1, $string_2) {
  return (
      mb_strtoupper(preg_replace("/[^A-Za-z0-9]/", "", $string_1))
      === mb_strtoupper(preg_replace("/[^A-Za-z0-9]/", "", $string_2))
  );
}

function shoutString($string)
{
  if (Str::endsWith($string, '.'))
    $string = Str::replaceLast('.', '', $string);

  return mb_strtoupper(Str::finish($string, '!'));
}
