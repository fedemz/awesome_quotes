<?php

namespace App\Repositories;

use Storage;

class QuotesRepository implements IQuotesRepository
{

  /**
   * Returns no more than $count quotes from $name from one or more sources of data
   *
   * @param  int  $name
   * @param  int  $count
   * @return string
   */
  public function filter($name, $count)
  {
    // get quotes from different sources here
    $filtered_quotes = $this->getFileQuotes($name, $count);

    return json_encode(
        array_slice($filtered_quotes, 0, $count),
        JSON_UNESCAPED_UNICODE
    );
  }

  /**
   * Returns an array with no more than $count quotes from $name
   * extracted from quotes.json local file
   *
   * @param  int  $name
   * @param  int  $count
   * @return string[]
   */
  protected function getFileQuotes($name, $count)
  {
      $file_content = json_decode(Storage::disk('local')->get('quotes.json'), true);

      $filtered_quotes = [];
      foreach ($file_content as $quotes) {
          foreach ($quotes as $quote_array) {
              if (compareStringsUrl($quote_array['author'], $name)) {
                  array_push($filtered_quotes, shoutString($quote_array['quote']));
                  $count--;
                  if ($count == 0)
                      break;
              }
          }
      }

      return $filtered_quotes;
  }

}
