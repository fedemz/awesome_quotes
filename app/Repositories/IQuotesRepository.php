<?php

namespace App\Repositories;

interface IQuotesRepository
{

  /**
   * Returns no more than $count quotes from $name from one or more sources of data
   *
   * @param  int  $name
   * @param  int  $count
   * @return string
   */
  public function filter($name, $count);

}
