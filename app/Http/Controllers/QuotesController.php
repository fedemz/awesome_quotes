<?php

namespace App\Http\Controllers;

use App\Repositories\IQuotesRepository;

class QuotesController extends Controller
{
    protected $quotes;

    public function __construct(IQuotesRepository $quotes)
    {
        $this->quotes = $quotes;
    }

    /**
     * Returns $count quotes from author $count
     *
     * @param  int  $name
     * @param  int  $count
     * @return \Illuminate\Http\Response
     */
    public function filter($name, $count)
    {
        if ($count >= 1 && $count <= 10) {
          return $this->quotes->filter($name, $count);
        }
        return response()->json(
            ['message' => 'Error! Number of quotes must be between 1 and 10'],
            412
        );
    }

}
