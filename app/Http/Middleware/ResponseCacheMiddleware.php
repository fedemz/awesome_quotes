<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Cache;

class ResponseCacheMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      return Cache::remember(request()->url(), env('API_CACHE_TIME'), function () use ($request, $next) {
          return $next($request);
      });
    }
}
