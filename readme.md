# Awesome Quotes Laravel API

## Prerequisites
- Vagrant
- Virtualbox

## Installation:
1. clone project
2. add **192.168.10.10 api.awesomequotes.test** to **/etc/hosts**
3. run **php composer.phar install**
4. run **vagrant up**

## Usage:
GET:

    http://api.awesomequotes.test/quotes/[author]/[count]

----
Notes:

- [author] field is case insensitive and doesn't consider spaces.
All of these examples will work: steve-jobs, STEVEJOBS, Steve Jobs, steve----jobs.

- In case there aren't enough quotes to meet the client's request, all quotes from the author will be returned.


## Cache:
- To set cache time change **API\_CACHE_TIME** in **.env** file
- To clear cache run **php artisan cache:clear**

## Testing:
To run PHPUnit tests use the following command:

    vendor/bin/phpunit
