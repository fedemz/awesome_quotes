<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Compares expected vs real http response
     *
     * @return void
     */
    protected function checkResponse($url, $status, $msg)
    {
        $response = $this->get($url);
        $response->assertStatus($status);
        $this->assertEquals($response->decodeResponseJson(), $msg);
    }
}
