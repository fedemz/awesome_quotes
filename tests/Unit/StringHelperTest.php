<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StringHelperTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function compareStringsUrlTest()
    {
        $equalAuthors = [
            ["Steve-Jobs", "Steve Jobs"],
            ["steve-jobs", "Steve-Jobs"],
            ["steve-jobs", "stevejobs"],
            ["W. Clement Stone", "W-Clement-Stone"],
            ["W. Clement Stone", "wclementstone"],
            ["Martin Luther King Jr.", "Martinlutherkingjr"],
            ["vincent van Gogh", "vincent-Van Gogh"],
            ["audreyhepburn", "–Audrey Hepburn"]
        ];

        $notEqualAuthors = [
            ['', 'Steve Jobs'],
            ['steve', ''],
            ['Steve-Jobs', 'Steve JobsS'],
            ['vincent van gogh', 'vincent van'],
            ['Henry David Thoreau', 'Henry Ford'],
            ["Steve-Jobs", "Steve-Jobs2"]
        ];

        foreach ($equalAuthors as $authors) {
            $this->assertTrue(compareStringsUrl($authors[0], $authors[1]));
            $this->assertTrue(compareStringsUrl($authors[1], $authors[0]));
        }

        foreach ($notEqualAuthors as $authors) {
            $this->assertFalse(compareStringsUrl($authors[0], $authors[1]));
            $this->assertFalse(compareStringsUrl($authors[1], $authors[0]));
        }
    }

    /**
     * @test
     *
     * @return void
     */
    public function shoutStringTest()
    {
        $shoutQuotes = [
            ["The only way to do great work is to love what you do.",
            "THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!"
            ],
            ["Your time is limited, so don’t waste it living someone else’s life!",
            "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!"
            ],
            ["asd. Rnkns!, Lóé. & sad. ;,P p Ññ Xxm!",
            "ASD. RNKNS!, LÓÉ. & SAD. ;,P P ÑÑ XXM!"
            ]
        ];

        foreach ($shoutQuotes as $quotes) {
            $this->assertEquals(shoutString($quotes[0]), $quotes[1]);
        }
    }
}
