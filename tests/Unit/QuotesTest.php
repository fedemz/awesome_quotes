<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuotesTest extends TestCase
{
    /**
     * @test
     *
     * Test with quotes from storage/app/quotes.json
     *
     * @return void
     */
    public function getQuotesTest()
    {
        $allQuotes = array(
            array(
                'author' => 'steve-jobs',
                'count' => 2,
                'quotes' => [
                    "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!",
                    "THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!"
                ]
            ),
            array(
                'author' => 'steve-jobs',
                'count' => 1,
                'quotes' => [
                    "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!"
                ]
            ),
            array(
                'author' => 'audrey-hepburn',
                'count' => 1,
                'quotes' => [
                    "NOTHING IS IMPOSSIBLE, THE WORD ITSELF SAYS, “I’M POSSIBLE!”!"
                ]
            ),
            array(
                'author' => 'jesus',
                'count' => 10,
                'quotes' => [
                    "ASK AND IT WILL BE GIVEN TO YOU; SEARCH, AND YOU WILL FIND; KNOCK AND THE DOOR WILL BE OPENED FOR YOU!"
                ]
            ),
            array(
                'author' => 'greta-thunberg',
                'count' => 2,
                'quotes' => []
            )
        );

        foreach ($allQuotes as $authorQuotes) {
            $url = 'http://'.env('API_DOMAIN').'/quotes/'.$authorQuotes['author'].'/'.$authorQuotes['count'];
            $this->checkResponse($url, 200, $authorQuotes['quotes']);
        }
    }
}
