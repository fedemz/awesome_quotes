<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiUrlsTest extends TestCase
{
    /**
     * @test API url. Ok request
     *
     * @return void
     */
    public function successUrlTest()
    {
        $response = $this->get('http://'.env('API_DOMAIN').'/quotes/author/5');
        $response->assertStatus(200);
    }

    /**
     * @test API url. Too many quotes asked
     *
     * @return void
     */
    public function countUrlTest()
    {
        $error_msg = [
            'message' => 'Error! Number of quotes must be between 1 and 10'
        ];

        $urls = [
            'http://'.env('API_DOMAIN').'/quotes/author/11',
            'http://'.env('API_DOMAIN').'/quotes/author/0',
            'http://'.env('API_DOMAIN').'/quotes/author/-1'
        ];

        foreach ($urls as $url) {
            $this->checkResponse($url, 412, $error_msg);
        }
    }

    /**
     * @test API url. URL not found
     *
     * @return void
     */
    public function notFoundUrlTest()
    {
        $error_msg = ['message' => '404 Not Found.'];

        $urls = [
            'http://'.env('API_DOMAIN'),
            'http://'.env('API_DOMAIN').'/quotes',
            'http://'.env('API_DOMAIN').'/quotes/author',
            'http://'.env('API_DOMAIN').'/quotes/author/4/1'
        ];

        foreach ($urls as $url) {
            $this->checkResponse($url, 404, $error_msg);
        }
    }
}
